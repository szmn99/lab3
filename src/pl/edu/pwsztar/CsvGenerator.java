package pl.edu.pwsztar;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

class CsvGenerator implements FileGenerator {

    private FileWriter CsvW ;

    public void setCsvW(){
        try {
            CsvW = new FileWriter("new.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void toCsv(List<String> lines)
    {
        try {
            setCsvW();
            stars();
            for (String line : lines) {
                CsvW.append(line);
                CsvW.append("\n");
            }
            stars();
            CsvW.flush();
            CsvW.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void toPdf(List<String> lines) {
        // TODO?
    }

    @Override
    public void toXml(List<String> lines) {
        // TODO?
    }

    private void stars() throws IOException{
        CsvW.append("*************\n");
    }
}