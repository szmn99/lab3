package pl.edu.pwsztar;

import java.util.List;

interface FileGenerator {
    void toCsv(List<String> lines);
    void toPdf(List<String> lines);
    void toXml(List<String> lines);
}