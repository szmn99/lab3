package pl.edu.pwsztar;

public class MaterialPoint2D extends Point2D {

    private int i;
    private double v;
    private double v1;

    public MaterialPoint2D(double v, double v1) {
        super(v, v1);
    }

    public MaterialPoint2D(double v, double v1, int i) {
        super();
    }


    public MaterialPoint2D() {
        this(v,v1);
    }

    public double getV(){
        return v;
    }

    public double getV1(){
        return v1;
    }

    public int getMass(){
        return i;
    }
}
